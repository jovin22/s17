console.log("hello world")

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function cusInfo(){
		let fullName = prompt("Enter your name");
		let ageInfo = prompt("Enter your age");
		let locInfo = prompt("enter your location");

		console.log("hello, " + fullName);
		console.log("You are " + ageInfo + "years old.");
		console.log("you live in " + locInfo);

		alert("Thank you!")
	};
cusInfo();
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function favBand(){
		console.log("1. Alesana");
		console.log("2. Skylit drive");
		console.log("3. Escape the fate");
		console.log("4. New found glory");
		console.log("5. The All-American rejects");
	};
	favBand();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function favMovie(){
		const movie1 = "deadpool";
		const movie2 = "avenger";
		const movie3 = "thor";
		const movie4 = "Captain America";
		const movie5 = "Doctor Strange";

		let rating1 =  "85%";
		let rating2 = "91%";
		let rating3 = "77%";
		let rating4 = "79%";
		let rating5 ="89%";

		console.log("1. " + movie1);
		console.log("Rotten Tomatoes rating: "+ rating1);
		console.log("2. " + movie2);
		console.log("Rotten Tomatoes rating: "+ rating2);
		console.log("3. " + movie3);
		console.log("Rotten Tomatoes rating: "+ rating3);
		console.log("4. " + movie4);
		console.log("Rotten Tomatoes rating: "+ rating4);
		console.log("5. " + movie5);
		console.log("Rotten Tomatoes rating: "+ rating5);
	};
	favMovie();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


//printUsers();
function printUsers(){
let printFriends = //function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printUsers();


//console.log(friend1);
//console.log(friend2);
